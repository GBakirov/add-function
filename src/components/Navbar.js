import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="container d-flex justify-content-around mt-3">
      <Link to="/" className="text-center ">
        <h3 className="text-uppercase">Home</h3>
      </Link>
      <Link to="/StudentList" className="text-center ">
        <h3 className="text-uppercase">Manage</h3>
      </Link>
    </div>
  );
};

export default Navbar;
