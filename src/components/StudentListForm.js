import React, { Component } from "react";
// import Student from "./Student";
// import StudentAdd from "./StudentAdd";
import {
  AvForm,
  AvField,
  AvRadio,
  AvRadioGroup,
} from "availity-reactstrap-validation";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { AiFillEdit, AiFillDelete } from "react-icons/ai";

export default class StudentListForm extends Component {
  state = {
    modal: false,
    content: null,
    tempObject: null,
  };

  closeModal = () => {
    this.setState({ modal: !this.state.modal, tempObject: null });
  };

  handleValidSubmit = (events, value) => {
    const { addNewStudent, editStudent } = this.props;

    if (this.state.tempObject) {
      value.id = this.state.tempObject.id;
      editStudent(value);
      this.fetchData();
    } else {
      addNewStudent(value);
    }

    this.closeModal();
    this.fetchData();
  };

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    const { studentList } = this.props;

    this.setState({
      content: studentList.length ? (
        studentList.map((student, index) => (
          <div className="col-md-3 mb-4" key={index}>
            <div className="card card-body">
              <h6>Name: {student.name}</h6>
              <h6>Age: {student.age}</h6>
              <h6>jinsi: {student.jinsi}</h6>
              <h6>Job: {student.kasbi}</h6>
              <div className="d-flex justify-content-end align-items-center">
                <button
                  className="btn btn-warning mr-2"
                  type="click"
                  onClick={() => this.handleEdit(student)}
                >
                  <AiFillEdit color="white" />
                </button>
                <button
                  className="btn btn-danger"
                  type="click"
                  onClick={() => this.handleDelete(student.id)}
                >
                  <AiFillDelete color="white" />
                </button>
              </div>
            </div>
          </div>
          /* <Student
              key={index}
              studentList={studentList}
              setStudentList={setStudentList}
              student={student}
            /> */
        ))
      ) : (
        <h4 className="text-danger">No Student!!!</h4>
      ),
    });
  };

  handleDelete = (id) => {
    const { deleteStudent } = this.props;
    deleteStudent(id);
    this.fetchData();
  };

  handleEdit = (student) => {
    this.setState({ modal: !this.state.modal, tempObject: student });
  };
  render() {
    const { modal, content, tempObject } = this.state;

    return (
      <div className="container my-4">
        <div className="d-flex align-center justify-content-around">
          <h3>Student List</h3>
          <button className="btn btn-primary" onClick={this.closeModal}>
            Add Student
          </button>
        </div>
        <hr />
        <div className="d-flex flex-wrap justify-content-center my-4">
          {content}
        </div>
        {/* <StudentAdd
          modal={this.state.modal}
          closeModal={this.closeModal}
          addNewStudent={(object) => addNewStudent(object)}
        /> */}
        <div>
          <Modal isOpen={modal} toggle={this.closeModal}>
            <ModalHeader toggle={this.closeModal}>
              {tempObject ? "EDIT" : "ADD"} STUDENT
            </ModalHeader>
            <AvForm onValidSubmit={this.handleValidSubmit}>
              <ModalBody>
                <AvField
                  value={tempObject ? tempObject.name : ""}
                  name="name"
                  type="text"
                  label="Name"
                  required
                />
                <AvField
                  value={tempObject ? tempObject.age : ""}
                  name="age"
                  type="number"
                  label="Age"
                  required
                />
                <AvRadioGroup
                  value={tempObject ? tempObject.jinsi : ""}
                  inline
                  name="jinsi"
                  label="Jinsi"
                  required
                  className="d-flex align-items-center"
                >
                  <AvRadio type="radio" label="Erkak" value="E" />
                  <AvRadio type="radio" label="Ayol" value="A" />
                </AvRadioGroup>
                <AvField
                  value={tempObject ? tempObject.kasbi : ""}
                  type="text"
                  name="kasbi"
                  label="Kasbi"
                  required
                />
              </ModalBody>
              <ModalFooter>
                <Button color="primary" type="submit">
                  {tempObject ? "Save" : "Add"}
                </Button>{" "}
                <Button color="secondary" onClick={this.closeModal}>
                  {tempObject ? "Don't Save" : "Cancel"}
                </Button>
              </ModalFooter>
            </AvForm>
          </Modal>
        </div>
      </div>
    );
  }
}
