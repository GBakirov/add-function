const Home = ({ studentList }) => {
  return (
    <div className="container my-4">
      <h1 className="text-center">Student List</h1>
      <hr />
      <div className="d-flex flex-wrap justify-content-center my-4">
        {studentList.length ? (
          studentList.map((student, index) => (
            <div className="col-md-3 mb-4" key={index}>
              <div className="card card-body">
                <h6>Name: {student.name}</h6>
                <h6>Age: {student.age}</h6>
                <h6>jinsi: {student.jinsi}</h6>
                <h6>Job: {student.kasbi}</h6>
              </div>
            </div>
            /* <Student
              key={index}
              studentList={studentList}
              setStudentList={setStudentList}
              student={student}
            /> */
          ))
        ) : (
          <h4 className="text-danger">No Student!!!</h4>
        )}
      </div>
    </div>
  );
};

export default Home;
