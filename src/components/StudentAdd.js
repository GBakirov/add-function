import {
  AvForm,
  AvField,
  AvRadio,
  AvRadioGroup,
} from "availity-reactstrap-validation";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
const StudentAdd = ({ modal, closeModal, addNewStudent }) => {
  const handleValidSubmit = (events, value) => {
    addNewStudent(value);
    closeModal();
  };
  return (
    <div>
      <Modal isOpen={modal} toggle={closeModal}>
        <ModalHeader toggle={closeModal}>Add Student Form</ModalHeader>
        <AvForm onValidSubmit={handleValidSubmit}>
          <ModalBody>
            <AvField name="name" type="text" label="Name" required />
            <AvField name="age" type="number" label="Age" required />
            <AvRadioGroup
              inline
              name="jinsi"
              label="Jinsi"
              required
              className="d-flex align-items-center"
            >
              <AvRadio type="radio" label="Erkak" value="E" />
              <AvRadio type="radio" label="Ayol" value="A" />
            </AvRadioGroup>
            <AvField type="text" name="kasbi" label="Kasbi" required />
          </ModalBody>
          <ModalFooter>
            <Button color="primary" type="submit">
              Add
            </Button>{" "}
            <Button color="secondary" onClick={closeModal}>
              Cancel
            </Button>
          </ModalFooter>
        </AvForm>
      </Modal>
    </div>
  );
};

export default StudentAdd;
