const Student = ({ studentList, setStudentList, student }) => {
  const handleDelete = () => {
    setStudentList(studentList.filter((el) => el.id !== student.id));
  };
  return (
    <div className="col-md-3 mb-4">
      <div className="card card-body">
        <h6>Name: {student.name}</h6>
        <h6>Age: {student.age}</h6>
        <h6>jinsi: {student.jinsi}</h6>
        <h6>Job: {student.kasbi}</h6>
        <button className="btn btn-danger" type="click" onClick={handleDelete}>
          Delete
        </button>
      </div>
    </div>
  );
};

export default Student;
