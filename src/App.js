import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import StudentListForm from "./components/StudentListForm";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
function App() {
  const create_random_id = (lenght) => {
    var random_string = "";
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz*/&%$#@!";
    for (var i = 0; i < lenght; i++) {
      random_string += characters.charAt(
        Math.floor(Math.random() * characters.length)
      );
    }
    return random_string;
  };

  const [studentList, setStudentList] = useState([
    {
      id: create_random_id(5),
      name: "Abbosxon",
      age: 24,
      jinsi: "E",
      kasbi: "talaba",
    },
    {
      id: create_random_id(5),
      name: "Sevinchxon",
      age: 12,
      jinsi: "A",
      kasbi: "o`quvchi",
    },
  ]);

  const addNewStudent = (object) => {
    setStudentList([
      ...studentList,
      {
        id: create_random_id(5),
        name: object.name,
        age: object.age,
        jinsi: object.jinsi,
        kasbi: object.kasbi,
      },
    ]);
    console.log(studentList);
  };

  const getIndex = (id) => {
    let studentId = studentList.findIndex((obj) => obj.id === id);
    return studentId;
  };

  const deleteStudent = (id) => {
    studentList.splice(getIndex(id), 1);

    setStudentList(studentList);
  };
  const editStudent = (student) => {
    let studentId = getIndex(student.id);

    studentList[studentId].name = student.name;
    studentList[studentId].age = student.age;
    studentList[studentId].jinsi = student.jinsi;
    studentList[studentId].kasbi = student.kasbi;

    setStudentList(studentList);
  };

  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact={true} path="/">
          <Home studentList={studentList} setStudentList={setStudentList} />
        </Route>
        <Route path="/studentList">
          <StudentListForm
            studentList={studentList}
            addNewStudent={(object) => addNewStudent(object)}
            deleteStudent={(id) => deleteStudent(id)}
            editStudent={(student) => editStudent(student)}
          />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
